const express =  require('express');
const dotenv = require('dotenv');

//Configuration the .env file
dotenv.config();

// Create Express APP
const app = express();
const port = process.env.PORT || 8000;

//Define the first Route of APP
app.get('/',(req, res) => {
    // Send Hello 
    res.send('Welcome to APP RestFull Express + TS + Swager + Mongooose');
});
//eXECUTE APP AND LISTEN REQUEST TO PORT
app.listen(port,() => {
    console.log(`EXPRESS SERVER: RUNNING AT http://localhost:${port}`)
})