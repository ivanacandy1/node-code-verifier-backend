"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
//Configuration the .env file
dotenv_1.default.config();
// Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
/**
 *
 */
app.get('/', (req, res) => {
    // Send Hello 
    res.send('Welcome to Hola APP RestFull Express + TS + Swager + Mongooose');
});
app.get('/hello', (req, res) => {
    // Send Hello 
    res.send('Welcome to get route: Hello');
});
//eXECUTE APP AND LISTEN REQUEST TO PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: RUNNING AT http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map