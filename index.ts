import express, { Express, Request, Response } from "express";
import dotenv  from 'dotenv';


//Configuration the .env file
dotenv.config();

// Create Express APP
const app:Express = express();
const port:string | number = process.env.PORT || 8000;

/**
 * 
 */
app.get('/',(req: Request, res: Response) => {
    // Send Hello 
    res.send('Welcome to Hola APP RestFull Express + TS + Swager + Mongooose');
});
app.get('/hello',(req: Request, res: Response) => {
    // Send Hello 
    res.send('Welcome to get route: Hello');
});
//eXECUTE APP AND LISTEN REQUEST TO PORT
app.listen(port,() => {
    console.log(`EXPRESS SERVER: RUNNING AT http://localhost:${port}`)
})